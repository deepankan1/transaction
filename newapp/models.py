from django.db import models
from django.utils.translation import gettext as _


class Transaction (models.Model):
    created_at=models.DateField(_("created_at"))
    updated_at=models.DateField(_("updated_at"))
    is_active=models.BooleanField(_("is_active"))
    title=models.CharField(_("title"),max_length=35)
    amount=models.IntegerField(_("amount"))
    category_id=models.IntegerField(_("category_id"))
    user_id=models.SlugField(_("user_id"))
    transaction_date=models.DateField(_("transaction_date"))
    payment_type=models.CharField(_("payment_type"),max_length=35)
    remarks=models.SlugField(_("remarks"))
