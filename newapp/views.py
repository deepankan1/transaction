from django.db.models import Sum
from newapp.models import Transaction
from django.shortcuts import render

def example(request):
    data=Transaction.objects.all().aggregate(thedata=Sum('amount'))
    return render(request,'index.html',{"data":data})